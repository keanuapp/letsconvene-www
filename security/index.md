---
layout: subpage 
overtitle: Keanu Engine
title: Secure communications for your communities
description: Secure communications for your communities
featured_image: /assets/images/social.jpg
permalink: /security/
---

### Fall 2022 - v1.0.2

An overview of the privacy and security features, and other core project values, of the service offered at LetsConvene.im and other related domains.

## Privacy-Centered

The apps create places where you can be you with no strings attached—no personal phone numbers or email addresses are required.

* #### Free to Be You
You are represented by an account ID and QR code. Connect with people in seconds simply by clicking a link or scanning a code.

* #### No Strings Attached
No email or phone number is needed to join. Automated usernames are generated to encourage the use of random pseudonyms.

* #### No Data Mining
The apps don’t mine for user data. Rather, to learn from users, apps offer a privacy preserving way to learn from users what you need to. We process your data in compliance with GDPR regulations, only for specified and legitimate purposes, fairly, and in a transparent manner.

## Serious about security
Convene is designed to ensure the safety of each individual. Dive into the details of our secure setup for the apps on our security documentation.

* #### End-to-End Encryption
Convene is built on the Matrix protocol, an open network for secure, decentralized communication. Matrix’s state-of-the-art end-to-end encryption offers security on the device and in transit. Keys are generated and stored in the user’s local browser storage. All messages are encrypted and decrypted in-memory in the Convene web app running in the browser.

* #### Secure Infrastructure
The Convene team has experience deploying on Amazon Web Services, which is ISO certified for Cloud Security and Data Protection, Microsoft Azure Cloud, and other independent hosting providers. For network security purposes, we use web application firewalls to secure our services.  The operations can also deploy solutions in other environments offering a secure infrastructure, as requested.

* #### Data Storage
All servers are hosted and data is currently stored on servers either in the United States or Europe. In addition to servers, Convene sites utilize the Amazon Cloudfront Content Delivery Network, which may cache web traffic data in various network data centers between the service and the user. All message and media content stored is encrypted using cryptographic keys generated with the Matrix end-to-end encryption protocol. The keys are NOT stored in any server or data center, and are only resident in the user’s device or browser. When Convene rooms are deleted by the user, all message content and room metadata are deleted from the servers. When the user closes their browser tab or logouts of their Convene account, all local key and message cached data is deleted. No full IP addresses are logged by the server or analytics service, only country level information. Access logs are stored for the minimal amount of time necessary to operate the service, and are not shared with any third-party.

* #### Communication and Authentication
All internal and external communications are encrypted (TLS, VPN, SSH, OpenPGP, Signal, Matrix). All services require two-factor authentication access with hardware token, from authorized devices.

* #### Open & Accountable
The code is open source under the GNU Affero General Public License (AGPL v3.0), available on Gitlab under the “Keanu” codename: https://gitlab.com/keanuapp/ 

* #### Security Audits
Regular security audits and updates ensure that security standards are upheld. We use reputable third-party penetration testing teams to test the security of our services on an annual basis and/or after each major release. Audit reports available upon request. 

## Holistic Safety
Holistic digital security considers users’ experience in their physical environment as well as their digital.

* #### Anti-Censorship and Circumvention Capable
Services can be accessed through alternate and “mirror” domains to resist traffic surveillance and blocking.

* #### Self-Destructing
Easily delete rooms, wipe messages or all of the information from an app with one tap.

* #### Mindful of concerns
Each experience is carefully crafted to consider users’ behaviors and the threats they may be vulnerable to.

* #### Safe Spaces
Apps feature easy to join groups with workflows to ensure privacy. Learn more about privacy in semi-private groups.

* #### Trusted Relationships
Real identities aren’t linked. The apps deploy various methods for users to be confident that the people they are connected with are, in fact, whom they expect.

* #### Movement and Rights Defender Centered
The team aims to uphold the dignity of humankind and collaborates deeply with a number of leading human rights and humanitarian organizations. Learn more about the team at Guardian Project—People and code you can trust.

* #### Make it your own.
We’ll work with you to customize, brand, and deploy an instance of the service, with the requirements you have. All of the code and infrastructure is open-source and easy for any experienced team to work with and customize.

## Contact Us

Want to know more? Contact us [via email](mailto:sales@letsconvene.im) or learn more about the product team at [guardianproject.info](https://guardianproject.info)

